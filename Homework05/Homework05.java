import java.util.Scanner;

class Homework05 {
    public static int minNumber(int a) {
        if (a < 0) {
            a *= -1;
        }
        int min = a % 10;
        if (a / 10 == 0) {
               return a;
        }
        int ostatok = min;
        while (a != 0) {
            min = a % 10;
            if (min < ostatok) {
                ostatok = min;
            }
            a /= 10;

        }
        return ostatok;
    }


    public static void main(String[] args) {
         Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int min = 9;
        while (a != -1) {
            int ostatok = minNumber(a);
            if (ostatok < min) {
                min = ostatok;
            }
            a = scanner.nextInt();
        }
        System.out.println(min);

    }
}

