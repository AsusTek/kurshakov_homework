import java.util.Arrays;

class Homework06 {
    public static int getIndex(int[] array, int number) {
        int x = -1;
            for (int i = 0; i < array.length; i++) {
            if (number == array[i]) {
                return i;
            }
        }
        return x;
    }

    public static void shiftNull(int[] array) {
            int s = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0 && s == -1)
                s = i;
            if (array[i] != 0 && s != -1) {
                array[s] = array[i];
                array[i] = 0;
                i = s;
                s = -1;
            }
        }

    }


    public static void main(String[] args) {
        int[] a = {21, 0, 5, 7, 0, 17, 0, 83, 0,102};
        int[] b = {0, 180, 13, 0, 21, 55, 102, 0, 0};
        int result = getIndex(a,80);
        int result1 = getIndex(b,100);
        System.out.println(result);
        System.out.println(result1);
        shiftNull(a);
        shiftNull(b);
        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(b));
    }

}