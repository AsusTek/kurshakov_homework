package Homework07;

import java.util.Scanner;

public class Homework07 {

    public static int searchMinRepeat(int[] array) {
        int min = Integer.MAX_VALUE;
        int result = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0 && array[i] < min) {
                min = array[i];
                result = i - 100;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] numsCount = new int[201];
        int num = scanner.nextInt();
        while (num != -1) {
            numsCount[num + 100] += 1;
            num = scanner.nextInt();
        }
        System.out.println("Число, присутствуещее в последовательности минимальное количество раз: " + searchMinRepeat(numsCount));

        for (int i = 0; i < numsCount.length; i++) {
            if (numsCount[i] != 0) {
                System.out.printf("Индекс: %3d; число: %3d; количество: %d\n", i, (i - 100), numsCount[i]);
            }
        }
    }
}


