package Homework13;

import java.util.ArrayList;
import java.util.List;

public class Sequence {

    public static int[] fromList(List list) {
        int[] result = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            result[i] = (int) list.get(i);
        }
        return result;
    }

    public static int[] filter(int[] array, ByCondition condition) {
        List list = new ArrayList();
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                list.add(array[i]);
            }
        }
        return fromList(list);
    }

}
