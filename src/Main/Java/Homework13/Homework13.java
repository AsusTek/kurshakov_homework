package Homework13;

public class Homework13 {
    public static void main(String[] args) {
        int[] example1 = {34, 11, 52, 3, 88, 21, 100, 33, 90, 41, 66};
        int[] example2 = {15, 34, 66, 56, 46, 21, 99, 1, 260, 310, 9};
        int[] result1 = Sequence.filter(example1, (element) -> (element % 2 == 0));
        for (int i = 0; i < result1.length; i++) {
            System.out.print(result1[i] + " ");
        }
        int[] result2 = Sequence.filter(example2, (element) -> {
            int sum = 0;
            while (element > 10) {
                sum += element % 10;
                element = element / 10;
            }
            if ((sum + element) % 2 != 0) {
                return false;
            } else {
                return true;
            }
        });
        System.out.println();
        for (int i = 0; i < result2.length; i++) {
            System.out.print(result2[i] + " ");
        }
    }

}
