package Homework11;

public class Logger {

    private static Logger logger;
    private static String Logfile = "This is log file. \n\n";

    private Logger() {

    }

    public static Logger getLogger() {
        if (logger == null) {
            logger = new Logger();
        }
        return logger;
    }

    public void addLogInfo(String loginfo) {
        Logfile += loginfo + "\n";
    }

    public void showLogfile() {
        System.out.println(Logfile);
    }
}


